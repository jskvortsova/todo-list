import com.personal.entity.ListEntity
import com.personal.entity.ListItemEntity
import com.personal.model.TodoListDTO
import com.personal.repository.ListRepository
import com.personal.service.ListService
import com.personal.service.ListServiceImpl
import org.spockframework.util.Assert
import spock.lang.Specification

// TODO add more tests
class ListServiceTest extends Specification {

    ListRepository listRepository = Mock(ListRepository)
    ListService listService = new ListServiceImpl(listRepository)

    def setup(){
        ListEntity listEntity = new ListEntity(id: "id", name: "Things to buy", username: "username", listItems: [])
        def tasks = [
                new ListItemEntity("Car"),
                new ListItemEntity("House"),
                new ListItemEntity("Helicopter")
        ]
        listEntity.listItems = tasks

        listRepository.findAll() >> [listEntity]
        listRepository.findOne(_) >> listEntity
        listRepository.save(_) >> {args -> args[0]}
        listRepository.save(_) >> {args -> args[0]}
    }

    def "Create list"(){
        when:
        TodoListDTO list = listService.create("New list", "userID")
        then:
        Assert.that(list.name == "New list")
    }

    def "Remove list"(){
        when:
        Boolean result = listService.remove("id")
        then:
        Assert.that(result)
    }

    def "Get by Id"(){
        given:
        def id = "id"
        when:
        ListEntity list = listService.getListById(id)
        then:
        Assert.that(list.id == id)
    }
}
