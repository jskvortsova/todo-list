package com.personal.controller;

import com.personal.entity.ListItemEntity;
import com.personal.model.ListItemDTO;
import com.personal.model.TodoListDTO;
import com.personal.service.ListService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.List;

@Controller
public class TaskController {

    @Autowired
    private ListService listService;

    @RequestMapping(value = "/task/add", method = RequestMethod.POST)
    @ResponseBody
    public ListItemDTO addItem(@RequestParam("listId") String listId, @RequestBody String text){
        ListItemEntity listItemEntity = listService.addTask(listId, new ListItemEntity(text));
        return new ListItemDTO(listItemEntity, listId);
    }

    @RequestMapping(value = "/task/remove", method = RequestMethod.POST)
    @ResponseBody
    public TodoListDTO removeItem(@RequestParam("listId") String listId, @RequestBody String id){
        return new TodoListDTO(listService.removeTask(listId, id));
    }

    @RequestMapping(value = "/task/finish", method = RequestMethod.POST)
    @ResponseBody
    public TodoListDTO finishItem(@RequestParam("listId") String listId, @RequestBody String id){
        return new TodoListDTO(listService.finishTask(listId, id));
    }
}
