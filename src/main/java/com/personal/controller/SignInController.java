package com.personal.controller;

import com.personal.model.UserDTO;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class SignInController {

    @RequestMapping(value = {"/signin"}, method = RequestMethod.GET)
    public String showLoginPage(Model model, String error, String logout){
        if (error != null)
        {
            model.addAttribute("error", "Your username and password is invalid.");
        }

        if (logout != null)
        {
            model.addAttribute("message", "You have been logged out successfully.");
        }

        model.addAttribute("user", new UserDTO.Credentials());
        return "signin";
    }
}
