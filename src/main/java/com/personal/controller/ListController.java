package com.personal.controller;

import com.personal.entity.ListItemEntity;
import com.personal.model.ListItemDTO;
import com.personal.model.TodoListDTO;
import com.personal.service.ListService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.List;

@Controller
public class ListController {

    @Autowired
    private ListService listService;

    @RequestMapping(value = "/lists", method = RequestMethod.GET)
    public String showLists(Model model, Principal principal){
        String username = principal.getName();
        model.addAttribute("username", username);
        List<TodoListDTO> lists = listService.getListsByUsername(username);
        model.addAttribute("lists", lists);
        return "lists";
    }
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public String showList(Model model, Principal principal, @RequestParam(name = "id") String id){
        String username = principal.getName();
        model.addAttribute("username", username);
        TodoListDTO list = new TodoListDTO(listService.getListById(id));
        model.addAttribute("list", list);
        return "list";
    }

    //TODO create custom response with error and handle errors properly on UI

    @RequestMapping(value = "/lists/remove", method = RequestMethod.POST)
    @ResponseBody
    public Boolean remove(@RequestBody String id){
        return listService.remove(id);
    }

    @RequestMapping(value = "/lists/add", method = RequestMethod.POST)
    @ResponseBody
    public TodoListDTO createList(Principal principal, @RequestBody String name){
        TodoListDTO todoListDTO = listService.create(name, principal.getName());
        return todoListDTO;
    }
}
