package com.personal.controller;

import com.personal.auth.UserValidator;
import com.personal.auth.service.SecurityService;
import com.personal.model.UserDTO;
import com.personal.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Controller
public class RegistrationController {

    @Autowired
    private UserService userService;

    @Autowired
    private SecurityService securityService;

    @Autowired
    private UserValidator userValidator;

    @RequestMapping(value = "/register", method = RequestMethod.GET)
    public String showRegistrationPage(Model model){
        model.addAttribute("user", new UserDTO());
        return "register";
    }

    @RequestMapping(value = "/register", method = RequestMethod.POST)
    public String register(Model model, RedirectAttributes redirectAttributes, @ModelAttribute("user") UserDTO userDTO, BindingResult bindingResult){
        userValidator.validate(userDTO, bindingResult);

        if (bindingResult.hasErrors()) {
            return "register";
        }
        userService.save(userDTO);
        securityService.autologin(userDTO.getCredentials().getUsername(), userDTO.getCredentials().getPassword());
        redirectAttributes.addFlashAttribute("user", userDTO.getCredentials());
        return "redirect:/lists";
    }
}
