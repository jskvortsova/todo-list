package com.personal.auth;

import com.personal.entity.UserEntity;
import com.personal.model.UserDTO;
import com.personal.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

@Component
public class UserValidator implements Validator {
    @Autowired
    private UserService userService;

    @Override
    public boolean supports(Class<?> aClass) {
        return UserDTO.class.equals(aClass);
    }

    @Override
    public void validate(Object o, Errors errors) {
        UserDTO user = (UserDTO) o;

        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "credentials.username", "NotEmpty");
        if (user.getCredentials().getUsername().length() < 6 || user.getCredentials().getUsername().length() > 32) {
            errors.rejectValue("credentials.username", "Size.userForm.username");
        }
        if (userService.findByLogin(user.getCredentials().getUsername()) != null) {
            errors.rejectValue("credentials.username", "Duplicate.userForm.username");
        }

        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "credentials.password", "NotEmpty");
        if (user.getCredentials().getPassword().length() < 8 || user.getCredentials().getPassword().length() > 32) {
            errors.rejectValue("credentials.password", "Size.userForm.password");
        }
    }
}