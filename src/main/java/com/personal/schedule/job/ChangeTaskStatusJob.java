package com.personal.schedule.job;

import com.personal.model.ListItemDTO;
import com.personal.service.ListService;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ChangeTaskStatusJob implements Job {

    @Autowired
    private ListService listService;

    @Override
    public void execute(JobExecutionContext context) throws JobExecutionException {
        listService.markTasksWithStatus(ListItemDTO.Status.DUE);
    }
}
