package com.personal.schedule.config;

import com.personal.schedule.job.ChangeTaskStatusJob;
import org.quartz.*;
import org.quartz.impl.StdSchedulerFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.quartz.SpringBeanJobFactory;

import java.io.IOException;

import static org.quartz.JobBuilder.newJob;
import static org.quartz.SimpleScheduleBuilder.simpleSchedule;
import static org.quartz.TriggerBuilder.newTrigger;

@Configuration
public class SchedulerConfiguration {

    Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired
    private ApplicationContext applicationContext;

    @Bean
    public SpringBeanJobFactory springBeanJobFactory() {
        AutoWiringSpringJobFactory jobFactory = new AutoWiringSpringJobFactory();
        logger.debug("Configuring Job factory");

        jobFactory.setApplicationContext(applicationContext);
        return jobFactory;
    }

    @Bean
    public Scheduler scheduler(Trigger trigger, JobDetail job) throws SchedulerException, IOException {

        SchedulerFactory factory = new StdSchedulerFactory();

        logger.debug("Getting a handle to the Scheduler");
        Scheduler scheduler = factory.getScheduler();
        scheduler.setJobFactory(springBeanJobFactory());
        scheduler.scheduleJob(job, trigger);

        logger.debug("Starting Scheduler threads");
        scheduler.start();
        return scheduler;
    }

    @Bean
    public JobDetail jobDetail() {

        return newJob()
                .ofType(ChangeTaskStatusJob.class)
                .storeDurably()
                .withIdentity(JobKey.jobKey("Change_Task_Status_Job"))
                .build();
    }

    @Bean
    public Trigger trigger(JobDetail job) {

        int frequencyInHours = 24;
        logger.info("Configuring trigger to fire every {} hours", frequencyInHours);

        return newTrigger()
                .forJob(job)
                .withIdentity(TriggerKey.triggerKey("Change_Task_Status_Trigger"))
                .withSchedule(simpleSchedule()
                        .withIntervalInMinutes(frequencyInHours)
                        .repeatForever())
                .build();
    }
}