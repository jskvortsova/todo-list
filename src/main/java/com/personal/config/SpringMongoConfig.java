package com.personal.config;

import com.mongodb.Mongo;
import com.mongodb.MongoClient;
import com.mongodb.MongoCredential;
import com.mongodb.ServerAddress;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.config.AbstractMongoConfiguration;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

import java.util.Arrays;

@Configuration
@EnableMongoRepositories(basePackages = "com.personal")
@ComponentScan(basePackages = "com.personal")
public class SpringMongoConfig extends AbstractMongoConfiguration {


    @Value("${mongo.host}")//read from config
    private String host;

    @Value("${mongo.port}")//read from config
    private int port;

    @Value("${mongo.db}")//read from config
    private String databaseName;

//    @Value("${mongo.username}")//read from config
//    private String userName;
//
//    @Value("${mongo.password}")//read from config
//    private String password;


    @Override
    public String getDatabaseName() {
        return databaseName;
    }

    @Override
    @Bean
    public Mongo mongo() throws Exception {
        return new MongoClient(this.host, this.port);
    }

    @Bean
    public ServerAddress serverAddress() throws Exception {
        return new ServerAddress();//TODO: read host+port from config
    }

//    @Bean
//    public MongoCredential mongoCredential() {
//        return MongoCredential.createCredential(
//                this.userName,
//                getDatabaseName(),
//                this.password.toCharArray());
//    }

}