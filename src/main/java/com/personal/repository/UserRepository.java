package com.personal.repository;

import com.personal.entity.UserEntity;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;

public interface UserRepository extends MongoRepository<UserEntity, Long>, QueryDslPredicateExecutor<UserEntity> {

}
