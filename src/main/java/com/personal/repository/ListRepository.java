package com.personal.repository;

import com.personal.entity.ListEntity;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;
import org.springframework.stereotype.Repository;

@Repository
public interface ListRepository extends MongoRepository<ListEntity, Long>, QueryDslPredicateExecutor<ListEntity> {
}
