package com.personal.service;

import com.personal.entity.UserEntity;
import com.personal.model.UserDTO;

public interface UserService {
    UserDTO save(UserDTO userDTO);
    UserEntity findByLogin(String login);
}
