package com.personal.service;

import com.personal.entity.*;
import com.personal.model.ListItemDTO;
import com.personal.model.TodoListDTO;
import com.personal.repository.ListRepository;
import com.querydsl.core.types.dsl.BooleanExpression;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Service
public class ListServiceImpl implements ListService {

    @Autowired
    private ListRepository listRepository;

    public ListServiceImpl(ListRepository listRepository) {
        this.listRepository = listRepository;
    }

    //TODO handle case when list doesn't exist
    @Override
    public Boolean remove(String id) {
        try {
            ListEntity listEntity = getListById(id);
            listRepository.delete(listEntity);
            return true;
        }
        catch (Exception ex){
            return false;
        }
    }

    @Override
    public TodoListDTO create(String name, String username) {
        ListEntity listEntity = new ListEntity();
        listEntity.setName(name);
        listEntity.setUsername(username);
        listEntity.setListItems(new ArrayList<>());
        ListEntity savedList = listRepository.save(listEntity);

        return new TodoListDTO(savedList);
    }

    @Override
    public ListEntity getListById(String id) {
        return listRepository.findOne(QListEntity.listEntity.id.eq(id));
    }

    @Override
    public List<TodoListDTO> getListsByUsername(String username) {
        return StreamSupport.stream(listRepository.findAll(QListEntity.listEntity.username.eq(username)).spliterator(), false)
                .map(l -> new TodoListDTO(l))
                .collect(Collectors.toList());
    }

    @Override
    public ListItemEntity addTask(String listId, ListItemEntity task) {
        ListEntity list = getListById(listId);
        list.getListItems().add(task);
        listRepository.save(list);
        return task;
    }

    @Override
    public ListEntity removeTask(String listId, String taskId) {
        ListEntity list = getListById(listId);
        list.getListItems().removeIf(t -> t.getId().equals(taskId));
        listRepository.save(list);
        return list;
    }

    @Override
    public ListEntity finishTask(String listId, String taskId) {
        ListEntity list = getListById(listId);
        list.getListItems().stream().filter(t -> t.getId().equals(taskId)).forEach(t -> t.setStatus(ListItemDTO.Status.DONE));
        listRepository.save(list);
        return list;
    }

    @Override
    public Boolean markTasksWithStatus(ListItemDTO.Status status) {
        long DAY_IN_MS = 1000 * 60 * 60 * 24;
        Date twoWeeksAgo = new Date(System.currentTimeMillis() - (7 * DAY_IN_MS));
        try {
            BooleanExpression expression = QListEntity.listEntity.listItems.any().date.lt(twoWeeksAgo).and(QListItemEntity.listItemEntity.status.ne(status));
            Iterable<ListEntity> listEntities = listRepository.findAll(expression);
            listEntities.forEach(l -> l.getListItems().stream().filter(i -> i.getDate().before(twoWeeksAgo)).forEach(i -> i.setStatus(status)));
            listRepository.save(listEntities);
            return true;
        }
        catch (Exception ex){
            return false;
        }
    }

    @Override
    public void rename(String id, String name) {
        ListEntity listEntity = getListById(id);
        listEntity.setName(name);
        listRepository.save(listEntity);
    }}
