package com.personal.service;

import com.personal.entity.QUserEntity;
import com.personal.entity.UserEntity;
import com.personal.model.UserDTO;
import com.personal.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    public UserRepository userRepository;

    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    @Override
    public UserDTO save(UserDTO userDTO) {
        userDTO.getCredentials().setPassword(bCryptPasswordEncoder.encode(userDTO.getCredentials().getPassword()));
        userRepository.save(new UserEntity(userDTO));
        return userDTO;
    }

    @Override
    public UserEntity findByLogin(String login) {
        return userRepository.findOne(QUserEntity.userEntity.login.eq(login));
    }

}
