package com.personal.service;

import com.personal.entity.ListEntity;
import com.personal.entity.ListItemEntity;
import com.personal.model.ListItemDTO;
import com.personal.model.TodoListDTO;

import java.util.List;

public interface ListService {
    Boolean remove(String id);
    TodoListDTO create(String name, String userId);
    void rename(String id, String name);
    ListEntity getListById(String id);
    List<TodoListDTO> getListsByUsername(String username);
    ListItemEntity addTask(String listId, ListItemEntity task);
    ListEntity removeTask(String listId, String taskId);
    ListEntity finishTask(String listId, String taskId);
    Boolean markTasksWithStatus(ListItemDTO.Status status);
}
