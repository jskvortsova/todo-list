package com.personal.entity;

import com.querydsl.core.annotations.QueryEntity;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.ArrayList;
import java.util.List;

@QueryEntity
@Document(collection = "lists")
public class ListEntity {

    @Id
    private String id;
    private String username;
    private String name;
    private List<ListItemEntity> listItems;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<ListItemEntity> getListItems() {
        if(listItems == null){
            listItems = new ArrayList<>();
        }
        return listItems;
    }

    public void setListItems(List<ListItemEntity> listItems) {
        this.listItems = listItems;
    }

    @Override
    public String toString() {
        return "ListEntity{" +
                "id='" + id + '\'' +
                ", username='" + username + '\'' +
                ", name='" + name + '\'' +
                ", listItems=" + listItems +
                '}';
    }
}
