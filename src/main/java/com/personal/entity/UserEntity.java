package com.personal.entity;

import com.querydsl.core.annotations.QueryEntity;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import com.personal.model.UserDTO;

import java.util.UUID;

@QueryEntity
@Document(collection = "users")
public class UserEntity {
    @Id
    private String id;
    private String name;
    private String password;
    private String login;

    public UserEntity() {
    }

    public UserEntity(UserDTO userDTO) {
        this.id = userDTO.getId();
        this.name = userDTO.getName();
        this.password = userDTO.getCredentials().getPassword();
        this.login = userDTO.getCredentials().getUsername();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }
}
