package com.personal.entity;


import com.personal.model.ListItemDTO;
import com.querydsl.core.annotations.QueryEntity;
import org.springframework.data.annotation.Id;

import java.util.Date;
import java.util.UUID;

@QueryEntity
public class ListItemEntity {

    @Id
    private String id;
    private Date date;
    private String text;
    private ListItemDTO.Priority priority;
    private ListItemDTO.Status status;

    public ListItemEntity(String text) {
        this.id = UUID.randomUUID().toString();
        this.text = text;
        this.date = new Date();
        this.priority = ListItemDTO.Priority.LOW;
        this.status = ListItemDTO.Status.NONE;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public ListItemDTO.Priority getPriority() {
        return priority;
    }

    public void setPriority(ListItemDTO.Priority priority) {
        this.priority = priority;
    }

    public ListItemDTO.Status getStatus() {
        return status;
    }

    public void setStatus(ListItemDTO.Status status) {
        this.status = status;
    }
}
