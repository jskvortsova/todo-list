package com.personal.model;

import com.fasterxml.jackson.annotation.JsonView;
import com.personal.entity.ListEntity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.toList;

public class TodoListDTO{
    private String id;
    private String name;
    private List<ListItemDTO> items;

    public TodoListDTO() {
    }

    public TodoListDTO(ListEntity listEntity) {
        this(listEntity.getName());
        this.setId(listEntity.getId());
        this.setItems(listEntity.getListItems()
                .stream()
                .map(item -> new ListItemDTO(item, listEntity.getId()))
                .collect(toList()));
    }

    public TodoListDTO(String name) {
        this.id = UUID.randomUUID().toString();
        this.name = name;
        this.items = new ArrayList<>();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<ListItemDTO> getItems() {
        if(items == null){
            items = new ArrayList<>();
        }
        return items;
    }

    public void setItems(List<ListItemDTO> items) {
        this.items = items;
    }

    @Override
    public String toString() {
        return "TodoListDTO{" +
                "name=" + name +
                ", items=" + items +
                '}';
    }
}