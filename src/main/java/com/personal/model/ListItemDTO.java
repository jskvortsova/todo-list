package com.personal.model;

import com.personal.entity.ListItemEntity;

import java.util.Date;
import java.util.UUID;

public class ListItemDTO {
    private String id;
    private String listId;
    private Date date;
    private String text;
    private Priority priority;
    private Status status;

    public ListItemDTO(String text, String listId) {
        this.id = UUID.randomUUID().toString();
        this.listId = listId;
        this.text = text;
        this.date = new Date();
        this.priority = Priority.LOW;
        this.status = Status.NONE;
    }

    public ListItemDTO(ListItemEntity listItemEntity, String listId) {
        this.id = listItemEntity.getId();
        this.listId = listId;
        this.text = listItemEntity.getText();
        this.date = listItemEntity.getDate();
        this.priority = listItemEntity.getPriority();
        this.status = listItemEntity.getStatus();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getListId() {
        return listId;
    }

    public void setListId(String listId) {
        this.listId = listId;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Priority getPriority() {
        return priority;
    }

    public void setPriority(Priority priority) {
        this.priority = priority;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "ListItemDTO{" +
                ", date=" + date +
                ", text='" + text + '\'' +
                ", priority=" + priority +
                ", status=" + status +
                '}';
    }

    public enum Priority {
        LOW, AVERAGE, HIGH
    }

    public enum Status {
        DONE, PENDING, NONE, DUE
    }
}
