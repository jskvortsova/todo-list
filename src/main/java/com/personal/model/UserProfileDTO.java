package com.personal.model;

import java.util.List;

public class UserProfileDTO {
    private String userId;
    private List<TodoListDTO> lists;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public List<TodoListDTO> getLists() {
        return lists;
    }

    public void setLists(List<TodoListDTO> lists) {
        this.lists = lists;
    }

    @Override
    public String toString() {
        return "UserProfileDTO{" +
                "userId='" + userId + '\'' +
                ", lists=" + lists +
                '}';
    }


}
