var classNames = {
    removeList: ".remove-list",
    openList: ".open-list"
}

var elementIds = {
    newListName: "#new-list-name",
    addList: "#add-list",
    newList: "#new-list"
}

$(document).ready(function(){
    $(classNames.newListName).focusout(function(){
        createList($(this).val());
    });
    $(classNames.newListName).keypress(function(e) {
         if(e.which == 13) {
              createList($(this).val());
         }
     });
    $(elementIds.removeList).click(deleteList);
    $(classNames.openList).click(openList);

    $(elementIds.addList).click(function(){
        $(elementIds.newList).toggle();
        $(elementIds.newListName).focus();
    });
});

function createList(text){

    if(text){
        $.ajax({
          contentType: 'application/json;charset=UTF-8',
          method: "POST",
          url: "/lists/add",
          dataType: "json",
          cache: false, // Force requested pages not to be cached by the browser
          processData: false,
          data: text,
          success: function(data){
            addList(data);
          },
          error: function(e){
            alert("error:" +e);
          }
        });
    }
}

function openList(){

    currentElement = $(this)
    id = $(currentElement).parent('div').attr('id');

    window.location.replace("/list?id=" + id);
}

function addList(data){
    li = jQuery('<li/>', {
            class: 'list-group-item'
        })
    divRow = jQuery('<div/>', {
                id: data.id,
                class: "row"
            })
    div = jQuery('<div/>', {
                class: "open-list col-lg-11 col-md-11",
                click: openList
            })
    divRemove = jQuery('<div/>', {
                class: "remove-list col-lg-1 col-md-1",
                click: deleteList
            })
    spanName = jQuery('<span/>', {
               text: data.name
           })
    spanIcon = jQuery('<span/>', {
               class: 'glyphicon glyphicon-trash pull-right'
           })
    divRow.append(div)
    divRow.append(divRemove)
    div.append(spanName)
    divRemove.append(spanIcon)
    li.append(divRow)
    li.insertBefore($(elementIds.newList))


    $(elementIds.newListName).val('');
    $(elementIds.newList).toggle();
}

function deleteList(){
    currentElement = $(this)
    id = $(currentElement).parent("div").attr('id');
    data = {id: id};

    if(id){
        $.ajax({
          contentType: 'application/json;charset=UTF-8',
          method: "POST",
          url: "/lists/remove",
          dataType: "json",
          cache: false, // Force requested pages not to be cached by the browser-->
          processData: false,
          data: id,
          success: function(data){
            if(data)
            {
                removeListElement($(currentElement))
            }
            else{
                alert("Cannot delete list");
            }
          },
          error: function(e){
            alert("error:"+e);
          }
        });
    } else {
        $(elementIds.newList).toggle();
    }
}

function removeListElement(currentElement){
     $(currentElement).parents("li").remove();
}