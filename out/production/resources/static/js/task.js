var classNames = {
    finishTask: ".finish-task",
    removeTask: ".remove-task"
}

var elementIds = {
    newTaskName: "#new-task-name",
    addTask: "#add-task",
    newTask: "#new-task"
}

$(document).ready(function(){
    $(elementIds.newTaskName).focusout(function(){
        createTask($(this))
    });
    $(elementIds.newTaskName).keypress(function(e) {
       if(e.which == 13) {
           createTask($(this));
       }
    });

    $(classNames.finishTask).dblclick(finishTask);

    $(classNames.removeTask).click(deleteTask);
    $(elementIds.addTask).click(function(){
        $(elementIds.newTask).toggle();
        $(elementIds.newTaskName).focus();
    });
});

function finishTask(){
     currentElement = $(this)
     listId = $(currentElement).parent("div").attr('listId');
     id = $(currentElement).parent("div").attr('id');
     if(id){
          $.ajax({
            contentType: 'application/json;charset=UTF-8',
            method: "POST",
            url: "/task/finish?listId=" + listId,
            dataType: "json",
            cache: false, // Force requested pages not to be cached by the browser-->
            processData: false,
            data: id,
            success: function(data){
              if(data)
              {
                  $(currentElement).addClass("done");
              }
              else{
                  alert("Cannot delete task");
              }
            },
            error: function(e){
              alert("error:"+e);
            }
          });
      }
}

function createTask(el){
    text = $(el).val();
    listId = $(el).attr('listId');
    data = { listId: "", text: text}
    if(text){
        $.ajax({
          contentType: 'application/json;charset=UTF-8',
          method: "POST",
          url: "/task/add?listId=" + listId,
          dataType: "json",
          cache: false, // Force requested pages not to be cached by the browser
          processData: false,
          data: text,
          success: function(data){
            addTask(data);
          },
          error: function(e){
            alert("error:" +e);
          }
        });
    }
}

function addTask(data){
    li = jQuery('<li/>', {
            class: 'list-group-item'
        })
    divRow = jQuery('<div/>', {
                id: data.id,
                listId: data.listId,
                class: "row"
            })
    div = jQuery('<div/>', {
                class: "col-lg-11 col-md-11 finish-task",
                dblclick: finishTask
            })
    divRemove = jQuery('<div/>', {
                class: "remove-task col-lg-1 col-md-1",
                click: deleteTask
            })
    spanName = jQuery('<span/>', {
               text: data.text
           })
    spanIcon = jQuery('<span/>', {
               class: 'glyphicon glyphicon-trash pull-right'
           })
    divRow.append(div)
    divRow.append(divRemove)
    div.append(spanName)
    divRemove.append(spanIcon)
    li.append(divRow)
    li.insertBefore($(elementIds.newTask))


    $(elementIds.newTaskName).val('');
    $(elementIds.newTaskName).focus();
}

function deleteTask(){
     currentElement = $(this)
     listId = $(currentElement).parent("div").attr('listId');
     id = $(currentElement).parent("div").attr('id');
     data = {id: id};

     if(id){
         $.ajax({
           contentType: 'application/json;charset=UTF-8',
           method: "POST",
           url: "/task/remove?listId=" + listId,
           dataType: "json",
           cache: false, // Force requested pages not to be cached by the browser-->
           processData: false,
           data: id,
           success: function(data){
             if(data)
             {
                 removeListElement($(currentElement))
             }
             else{
                 alert("Cannot delete task");
             }
           },
           error: function(e){
             alert("error:"+e);
           }
         });
     } else {
        $(elementIds.newTask).toggle();
     }
 }

function removeListElement(currentElement){
     $(currentElement).parents("li").remove();
}